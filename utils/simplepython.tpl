
{% extends 'templates/python/index.py.j2'%}

# markdown cells
{% block markdowncell scoped %}
{% set special_chars = "!@#$%^&*()-_+=[]{}|;:'\",.<>?/" %}
{%-for line in cell.source.split('\n')-%}
    {%-if not line.startswith("def main") or line.strip()[0:1] in special_chars -%}
        {# If the line starts with '#' #}
        {{-line|comment_lines-}}
    {%- else -%}
        {{-line-}}
    {% endif %}
{% endfor -%}
{% endblock markdowncell %}?

## Code cell with magic command
{% block codecell scoped %}
{%for line in cell.source.split('\n')%}
    {%-if line.startswith('%')-%}
        {# If the line starts with '%' #}
        {{-line | comment_lines-}}
    {% else %}
        {{-line-}}
    {% endif %}
{% endfor %}
{% endblock codecell %}

# rawcell
## Code cell with magic command
{% block rawcell  scoped %}
{{ cell.source}}
{% endblock rawcell  %}
