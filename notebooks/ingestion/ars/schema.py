from google.cloud.bigquery.schema import SchemaField


SCHEMAS = {
    "ads_performances": [
        SchemaField("campaign", "STRING", "NULLABLE"),
        SchemaField("platform", "STRING", "NULLABLE"),
        SchemaField("start_week", "DATE", "NULLABLE"),
        SchemaField("end_week", "DATE", "NULLABLE"),
        SchemaField("impressions", "INT64", "NULLABLE"),
        SchemaField("clicks", "INT64", "NULLABLE")
    ]
}
