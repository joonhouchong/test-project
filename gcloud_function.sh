#!/bin/sh
gcloud_login() {
    google-cloud-sdk/bin/gcloud auth activate-service-account --key-file $1
    google-cloud-sdk/bin/gcloud config set account $2
    google-cloud-sdk/bin/gcloud config set project $3
    google-cloud-sdk/bin/gcloud auth print-access-token | docker login -u oauth2accesstoken --password-stdin https://asia-southeast1-docker.pkg.dev    
}

deploy_python_script() {
    ls
    cd dockers
    pip install jupyter
    python deploy_notebook.py
}
